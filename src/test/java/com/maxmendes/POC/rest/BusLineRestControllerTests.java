package com.maxmendes.POC.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maxmendes.POC.controller.BusLineController;
import com.maxmendes.POC.model.BusLine;
import com.maxmendes.POC.service.BusLineService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class BusLineRestControllerTests {

    @Autowired
    private BusLineController busLineController;

    @MockBean
    private BusLineService busLineService;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private BusLine busLine;
    private List<BusLine> busLineList;

    @Before
    public void initOwners() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(busLineController)
                .setControllerAdvice(new Exception())
                .build();

        busLine = new BusLine();
        busLineList = new ArrayList<>();

        busLine.setId("5529");
        busLine.setCodigo("1493-1");
        busLine.setNome("ICARAI/MENINO DEUS");

        busLineList.add(busLine);

    }

    @Test
    public void testGeBusLineSuccess() throws Exception {

        this.mockMvc.perform(get("/api/linhas")
                .param("linha", "ICARAI/MENINO DEUS")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.[0].id").value("5529"))
                .andExpect(jsonPath("$.[0].codigo").value("1493-1"))
                .andExpect(jsonPath("$.[0].nome").value("ICARAI/MENINO DEUS"));;
    }


    @Test
    public void testCreateBusLineSuccess() throws Exception{

        BusLine newBusLine = busLineList.get(0);
        busLine.setId("5555555");
        ObjectMapper mapper = new ObjectMapper();
        String newBusLineAsJSON = mapper.writeValueAsString(newBusLine);
        this.mockMvc.perform(post("/api/linhas/")
                .content(newBusLineAsJSON).accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void testDeleteBusLineSuccess() throws Exception{
        try {
            this.mockMvc.perform(delete("/api/linhas")
                    .param("id", "5555555")
                    .accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testUpdateBusLineSuccess() throws Exception{
        BusLine newBusLine = busLineList.get(0);
        newBusLine.setNome("VILA MENINO DEUS");
        ObjectMapper mapper = new ObjectMapper();
        String newPetAsJSON = mapper.writeValueAsString(newBusLine);
        this.mockMvc.perform(put("/api/linhas")
                .content(newPetAsJSON).accept(MediaType.APPLICATION_JSON_VALUE).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

    }
}
