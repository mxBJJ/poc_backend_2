package com.maxmendes.POC.service;

import com.maxmendes.POC.model.Itinerary;
import com.maxmendes.POC.model.LatLng;
import com.maxmendes.POC.repository.ItineraryRepository;
import com.maxmendes.POC.repository.StopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItineraryService {

    private ItineraryRepository itineraryRepository;
    private StopRepository stopRepository;

    @Autowired
    public ItineraryService(ItineraryRepository itineraryRepository, StopRepository stopRepository) {
        this.itineraryRepository = itineraryRepository;
        this.stopRepository = stopRepository;
    }


    public Itinerary findById(String id){
        return itineraryRepository.findByIdLinha(id);
    }

    public void delete(String id){
        stopRepository.delete(id);
        itineraryRepository.delete(id);
    }

    public Itinerary save(Itinerary itinerary){

        itineraryRepository.save(itinerary);

        for(LatLng p : itinerary.getParadas()){
            stopRepository.save(itinerary.getIdLinha(), p.getLat(), p.getLng());
        }

        return itinerary;
    }


}
