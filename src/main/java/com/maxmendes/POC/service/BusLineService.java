package com.maxmendes.POC.service;

import com.maxmendes.POC.model.BusLine;
import com.maxmendes.POC.repository.BusLineSpringDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusLineService {

    private  BusLineSpringDataRepository busLineSpringDataRepository;


    @Autowired
    public BusLineService(BusLineSpringDataRepository busLineSpringDataRepository) {
        this.busLineSpringDataRepository = busLineSpringDataRepository;
    }

    public BusLine createBusLine(BusLine busLine){
        busLineSpringDataRepository.save(busLine);
        return busLine;
    }

    public List<BusLine> findAllBusLines(){
        return busLineSpringDataRepository.findAll();
    }

    public void deleteBusLine(String id){
        busLineSpringDataRepository.delete(id);
    }

    public void updateBusLine(String id, String codigo, String nome){
        busLineSpringDataRepository.update(id, codigo, nome);
    }
}
