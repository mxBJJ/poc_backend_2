package com.maxmendes.POC.model;

public class TaxiReponse {

    private Result result;

    public TaxiReponse() {
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
