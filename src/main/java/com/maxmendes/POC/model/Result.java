package com.maxmendes.POC.model;

import java.util.List;

public class Result {
    private List<TaxiPoint> records;


    public Result() {
    }

    public List<TaxiPoint> getRecords() {
        return records;
    }

    public void setRecords(List<TaxiPoint> records) {
        this.records = records;
    }
}
