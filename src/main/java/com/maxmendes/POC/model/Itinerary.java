package com.maxmendes.POC.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "itinerario")
public class Itinerary implements Serializable {

    @Id
    private String idLinha;

    private String nome;
    private String codigo;

    @JsonManagedReference
    @OneToMany(mappedBy = "itinerary")
    private List<LatLng> paradas;


    public Itinerary() {
    }


    public String getIdLinha() {
        return idLinha;
    }

    public void setIdLinha(String idLinha) {
        this.idLinha = idLinha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<LatLng> getParadas() {
        return paradas;
    }

    public void setParadas(List<LatLng> paradas) {
        this.paradas = paradas;
    }
}
