package com.maxmendes.POC.model;

import java.io.Serializable;

public class TaxiPointFromDisk implements Serializable {

    private Integer id;
    private String ponto;


    public TaxiPointFromDisk() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPonto() {
        return ponto;
    }

    public void setPonto(String ponto) {
        this.ponto = ponto;
    }
}
