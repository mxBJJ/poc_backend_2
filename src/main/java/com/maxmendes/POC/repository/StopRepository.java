package com.maxmendes.POC.repository;

import com.maxmendes.POC.model.LatLng;

public interface StopRepository {

    void save(String idLinha, String lat, String lng);
    void delete(String idLinha);
}
