package com.maxmendes.POC.repository;

import com.maxmendes.POC.model.Itinerary;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ItinerarySpringDataRepository extends ItineraryRepository, Repository<Itinerary, Long> {

    @Transactional
    @Query("SELECT i FROM Itinerary i WHERE i.idLinha = :idLinha")
    Itinerary findByIdLinha(@Param("idLinha") String id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Itinerary itinerario WHERE itinerario.id = :id")
    void delete(@Param("id") String id);
}
