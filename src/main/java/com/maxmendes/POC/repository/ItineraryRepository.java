package com.maxmendes.POC.repository;

import com.maxmendes.POC.model.BusLine;
import com.maxmendes.POC.model.Itinerary;
import org.springframework.dao.DataAccessException;

public interface ItineraryRepository {

    //CRUD operations
    Itinerary findByIdLinha(String id) throws DataAccessException;
    Itinerary save(Itinerary itinerary) throws DataAccessException;
    void delete(String id) throws DataAccessException;
}
