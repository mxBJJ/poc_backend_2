package com.maxmendes.POC.repository;

import com.maxmendes.POC.model.BusLine;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface BusLineRepository {

    //CRUD operations
    BusLine save(BusLine busLine) throws DataAccessException;
    BusLine findById(String id) throws DataAccessException;
    void update(String id, String codigo, String nome) throws DataAccessException;
    void delete(String id) throws DataAccessException;
    List<BusLine> findAll() throws DataAccessException;


}
