package com.maxmendes.POC.repository;

import com.maxmendes.POC.model.LatLng;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface StopSpringDataRepository extends StopRepository, Repository<LatLng, Long> {

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO paradas (itinerary_id, lat, lng) VALUES (:idLinha, :lat, :lng)", nativeQuery = true)
    void save(@Param("idLinha") String idLinha,@Param("lat") String lat, @Param("lng") String lng);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM paradas WHERE itinerary_id = :idLinha", nativeQuery = true)
    void delete(@Param("idLinha") String idLinha);
}
