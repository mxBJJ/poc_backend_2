package com.maxmendes.POC.repository;

import com.maxmendes.POC.model.BusLine;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface BusLineSpringDataRepository extends BusLineRepository, Repository<BusLine, Long>
{
    @Transactional
    @Modifying
    @Query("DELETE FROM BusLine linha WHERE linha.id = :id")
    void delete(@Param("id") String id);

    @Transactional
    @Modifying
    @Query("UPDATE BusLine linha SET linha.codigo = :codigo, linha.nome = :nome, linha.id = :id WHERE linha.id = :id")
    void update(@Param("id") String id,
                @Param("codigo") String codigo,
                @Param("nome") String nome);

}
