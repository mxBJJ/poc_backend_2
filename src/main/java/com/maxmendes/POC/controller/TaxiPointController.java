package com.maxmendes.POC.controller;

import com.maxmendes.POC.model.TaxiPointFromDisk;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Api("Taxi points.")
@RestController
@RequestMapping(value = "/api/pontostaxi")
public class TaxiPointController {

    @ApiOperation("read all taxi points")
    @GetMapping
    public ResponseEntity<List<TaxiPointFromDisk>> getAllTaxiPoints(){

        List<String> lines = new ArrayList<>();
        List<TaxiPointFromDisk> taxiPoints = new ArrayList<>();


        try {

            File myObj = new File("pontos_taxi");
            if (myObj.exists()) {
                System.out.println(myObj.getAbsolutePath());
                Scanner myReader = new Scanner(myObj);
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine();
                    System.out.println(data);
                    lines.add(data);
                }

                myReader.close();
            }else{
                if (myObj.createNewFile()) {
                    System.out.println("File created: " + myObj.getName());
                    System.out.println(myObj.getAbsolutePath());
                    System.out.println(myObj.getPath());

                }
            }
            } catch(IOException e){

                System.out.println("An error occurred or file not exists ...");

                System.out.println(e);
                return new ResponseEntity<List<TaxiPointFromDisk>>(HttpStatus.NOT_FOUND);
            }

            for (String s : lines) {
                TaxiPointFromDisk taxiPointFromDisk = new TaxiPointFromDisk();
                taxiPointFromDisk.setPonto(s);
                taxiPointFromDisk.setId(lines.indexOf(s));
                taxiPoints.add(taxiPointFromDisk);
            }


        return new ResponseEntity<List<TaxiPointFromDisk>>(taxiPoints, HttpStatus.OK);

    }

    @ApiOperation("create taxi point")
    @PostMapping
    public ResponseEntity<List<TaxiPointFromDisk>> createTaxiPoint(@RequestParam String ponto){


        List<String> lines = new ArrayList<>();
        List<TaxiPointFromDisk> taxiPoints = new ArrayList<>();

        try {
            File myObj = new File("pontos_taxi");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
                lines.add(data);
            }
            lines.add(ponto);
            myReader.close();
        } catch (IOException e) {
            System.out.println("File not exists.");
            System.out.println(getClass().getResource("pontos_taxi").getPath());
            return new ResponseEntity<List<TaxiPointFromDisk>>(HttpStatus.NOT_FOUND);

        }

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("pontos_taxi"));


            for(String s : lines){
                bw.write(s + "\n");
            }

            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        for(String s : lines){
            TaxiPointFromDisk taxiPointFromDisk = new TaxiPointFromDisk();
            taxiPointFromDisk.setPonto(s);
            taxiPointFromDisk.setId(lines.indexOf(s));
            taxiPoints.add(taxiPointFromDisk);

        }

        return new ResponseEntity<List<TaxiPointFromDisk>>(taxiPoints, HttpStatus.OK);
    }

}
