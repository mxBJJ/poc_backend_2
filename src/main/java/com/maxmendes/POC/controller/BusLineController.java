package com.maxmendes.POC.controller;

import com.maxmendes.POC.model.BusLine;
import com.maxmendes.POC.service.BusLineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;
import java.util.stream.Collectors;

@Api("Bus lines.")
@RestController
@RequestMapping(value = "/api/linhas")
public class BusLineController {


    @Autowired
    private BusLineService busLineService;


    @ApiOperation("create new bus line")
    @PostMapping (produces = "application/json")
    public ResponseEntity<BusLine> createNewBusLine(@RequestBody BusLine busLine){
        busLineService.createBusLine(busLine);
        return new ResponseEntity<BusLine>(busLine, HttpStatus.CREATED);
    }


    @ApiOperation("get bus line by idLinha")
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<BusLine>> getBusLineByIdLinha(@RequestParam String linha){

        UriComponents uri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("www.poatransporte.com.br")
                .path("/php/facades/process.php")
                .queryParam("a", "nc")
                .queryParam("p", "%")
                .queryParam("t", "o")
                .build();

        RestTemplate restTemplate = new RestTemplate();

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
        restTemplate.getMessageConverters().add(converter);

       ResponseEntity<BusLine[]> busLineResponse = restTemplate.getForEntity(uri.toUriString(), BusLine[].class);

       List<BusLine> busLines = new ArrayList<>();

       List<BusLine> busLinesFromDataBase = new ArrayList<>();

       List<BusLine> busLineList = new ArrayList<>();

       busLinesFromDataBase = busLineService.findAllBusLines();

       busLines = Arrays.asList(busLineResponse
               .getBody().clone());

       for (BusLine busLine : busLines){
            busLineList.add(busLine);
       }

        for(BusLine busLine : busLinesFromDataBase){
            busLineList.add(busLine);
        }

        if(busLines.size() == 0){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(busLineList.stream().filter(x -> x.getNome().contains(linha.toUpperCase()))
                .collect(Collectors.toList()));
    }

    @ApiOperation("update a bus line")
    @PutMapping(produces = "application/json")
    public ResponseEntity<Void> updateBusLine(@RequestBody BusLine busLine){

        busLineService.updateBusLine(busLine.getId(), busLine.getCodigo(), busLine.getNome());
        return ResponseEntity.noContent().build();
    }

    @ApiOperation("delete bus line by idLinha")
    @DeleteMapping
    public void deleteBusLine(@RequestParam String id){
        busLineService.deleteBusLine(id);
    }

}
