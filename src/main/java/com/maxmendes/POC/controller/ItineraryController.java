package com.maxmendes.POC.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maxmendes.POC.model.BusLine;
import com.maxmendes.POC.model.Itinerary;
import com.maxmendes.POC.model.LatLng;
import com.maxmendes.POC.service.BusLineService;
import com.maxmendes.POC.service.ItineraryService;
import com.maxmendes.POC.util.DistanceUtil;
import io.swagger.annotations.Api;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Api("Itineraries.")
@RestController
@RequestMapping(value = "/api/itinerario")
public class ItineraryController {


    @Autowired
    private ItineraryService itineraryService;

    @Autowired
    private BusLineService busLineService;


    @GetMapping
    public ResponseEntity<Itinerary> findAllItinerary(@RequestParam String id){

        UriComponents uri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("www.poatransporte.com.br")
                .path("/php/facades/process.php")
                .queryParam("a", "il")
                .queryParam("p", id)
                .build();

        RestTemplate restTemplate = new RestTemplate();

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
        restTemplate.getMessageConverters().add(converter);

        ResponseEntity<String> itinerary = restTemplate.getForEntity(uri.toUriString(), String.class);

        System.out.println(itinerary.getBody());



        Itinerary objItineary =new Itinerary();


        if(!itinerary.getBody().equals("{\"Linha n�o encontrada\"}")) {


            JSONObject jsonObj = new JSONObject(itinerary.getBody());

            objItineary.setIdLinha(jsonObj.get("idlinha").toString());
            objItineary.setCodigo(jsonObj.get("codigo").toString());
            objItineary.setNome(jsonObj.get("nome").toString());


            Integer i = 0;

            boolean hasKey = jsonObj.has(i.toString());

            List<LatLng> latLngList = new ArrayList<>();

            while (jsonObj.has(i.toString())) {

                byte[] jsonData = jsonObj.get(i.toString()).toString().getBytes();

                ObjectMapper mapper = new ObjectMapper();

                try {
                    LatLng point = mapper.readValue(jsonData, LatLng.class);

                    latLngList.add(point);

                } catch (IOException e) {
                    e.printStackTrace();
                    return new ResponseEntity<Itinerary>(HttpStatus.NOT_FOUND);
                }

                i++;
            }

            objItineary.setParadas(latLngList);
            return ResponseEntity.ok().body(objItineary);
        }

        if(this.itineraryService.findById(id) != null){
            objItineary = this.itineraryService.findById(id);
            return ResponseEntity.ok().body(objItineary);
        }

        return new ResponseEntity<Itinerary>(HttpStatus.NOT_FOUND);

    }


    @GetMapping(value = "/id")
    public ResponseEntity<Itinerary> findIniteraryById(@RequestParam String id){

        if(this.itineraryService.findById(id) == null){
            return new ResponseEntity<Itinerary>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Itinerary>(this.itineraryService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Itinerary> createNewItinerary(@RequestBody Itinerary itinerary){

        this.itineraryService.save(itinerary);

        if(this.itineraryService.findById(itinerary.getIdLinha()) == null){
            return new ResponseEntity<Itinerary>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<Itinerary>(itinerary, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Void> updateItinerary(@RequestBody Itinerary itinerary){
        if(this.itineraryService.findById(itinerary.getIdLinha()) == null){
            this.itineraryService.save(itinerary);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        }

        this.itineraryService.save(itinerary);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<Void> deleteItinerary(@RequestParam String id){
        this.itineraryService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    @GetMapping(value = "/raio")
    public ResponseEntity<List<BusLine>> busLinePerRadius(@RequestParam String lat, @RequestParam String lng, @RequestParam double raio){

        List<BusLine> response = new ArrayList<>();

        UriComponents uri = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host("www.poatransporte.com.br")
                .path("/php/facades/process.php")
                .queryParam("a", "nc")
                .queryParam("p", "%")
                .queryParam("t", "o")
                .build();

        RestTemplate restTemplate = new RestTemplate();

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
        restTemplate.getMessageConverters().add(converter);

        ResponseEntity<BusLine[]> busLineResponse = restTemplate.getForEntity(uri.toUriString(), BusLine[].class);

        List<BusLine> busLines = new ArrayList<>();

        List<BusLine> busLinesFromDataBase = new ArrayList<>();

        List<BusLine> busLineList = new ArrayList<>();

        busLinesFromDataBase = busLineService.findAllBusLines();

        busLines = Arrays.asList(busLineResponse
                .getBody().clone());

        for (BusLine busLine : busLines){
            busLineList.add(busLine);
        }

        for (BusLine busLine : busLinesFromDataBase){
            busLineList.add(busLine);
        }


        for(BusLine busLine : busLineList){

            UriComponents uri2 = UriComponentsBuilder.newInstance()
                        .scheme("http")
                        .host("www.poatransporte.com.br")
                        .path("/php/facades/process.php")
                        .queryParam("a", "il")
                        .queryParam("p", busLine.getId())
                        .build();

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            RestTemplate restTemplate2 = new RestTemplate();

            MappingJackson2HttpMessageConverter converter2 = new MappingJackson2HttpMessageConverter();
            converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
            restTemplate2.getMessageConverters().add(converter2);

            ResponseEntity<String> itinerary = restTemplate2.getForEntity(uri2.toUriString(), String.class);

            Itinerary objItineary =new Itinerary();

            JSONObject jsonObj = new JSONObject(itinerary.getBody());

            objItineary.setIdLinha(jsonObj.get("idlinha").toString());
            objItineary.setCodigo(jsonObj.get("codigo").toString());
            objItineary.setNome(jsonObj.get("nome").toString());


            Integer i = 0;

            boolean hasKey = jsonObj.has(i.toString());

            List<LatLng> latLngList = new ArrayList<>();

            while (jsonObj.has(i.toString())){

                byte[] jsonData = jsonObj.get(i.toString()).toString().getBytes();

                ObjectMapper mapper = new ObjectMapper();

                try {
                    LatLng point = mapper.readValue(jsonData, LatLng.class);

                    latLngList.add(point);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                i++;
            }

            objItineary.setParadas(latLngList);

            for(LatLng parada : objItineary.getParadas()){
                double radius = (DistanceUtil.distFrom(Double.parseDouble(lat),Double.parseDouble(lng)
                        ,Double.parseDouble(parada.getLat()),Double.parseDouble(parada.getLng())));

                if(radius <= raio){
                    if(!response.contains(busLine)) {
                        System.out.println(radius);
                        response.add(busLine);
                    }
                }
            }

        }

        return new ResponseEntity<List<BusLine>>(response, HttpStatus.OK);
    }

}
